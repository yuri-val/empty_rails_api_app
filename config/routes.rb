Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'routes#index'
  scope module: 'api' do
	namespace :v1 do
	end
  end
end
